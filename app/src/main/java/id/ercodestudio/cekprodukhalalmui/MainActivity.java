package id.ercodestudio.cekprodukhalalmui;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import id.ercodestudio.cekprodukhalalmui.Adapters.ProdukAdapter;
import id.ercodestudio.cekprodukhalalmui.Helper.RbHelper;
import id.ercodestudio.cekprodukhalalmui.Models.ProdukModel;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends BaseActivity {
    private AdView adBawah;
    private TextView tvTitle;
    private Button btnSearch;
    private Spinner spKategori;
    private EditText txtKeyword;
    private RecyclerView rvProduk;
    private ProdukAdapter adapter;
    private String KATEGORI = "", KEYWORD = "", PAGE = "0";
    private ArrayList<ProdukModel> ProdukModel = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupView();
        //getData();

    }

    private void setupView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

        adBawah = (AdView) findViewById(R.id.bannerBawah);
        AdRequest adReq = new AdRequest.Builder()
                .build();
        adBawah.loadAd(adReq);

        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvTitle.setText("Cek Produk Halal MUI");
        spKategori = (Spinner) findViewById(R.id.spKategori);
        spKategori.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                if (spKategori.getSelectedItem().toString().equalsIgnoreCase("nama produk")) {
                    KATEGORI = "nama_produk";
                } else if (spKategori.getSelectedItem().toString().equalsIgnoreCase("nama produsen")) {
                    KATEGORI = "nama_produsen";
                } else if (spKategori.getSelectedItem().toString().equalsIgnoreCase("nomor sertifikat")) {
                    KATEGORI = "nomor_sertifikat";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnSearch = (Button) findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.startAnimation(btnAnimasi);
                getData();
            }
        });

        txtKeyword = (EditText) findViewById(R.id.txtKeyword);
        rvProduk = (RecyclerView) findViewById(R.id.rvProduk);
        rvProduk.setLayoutManager(new LinearLayoutManager(c));
    }

    private void getData() {
        ProdukModel.clear();

        boolean cancel = false;
        View focusView = null;

        if (spKategori.getSelectedItem().toString().equalsIgnoreCase("kategori produk")) {
            Toast.makeText(c, "Silahkan pilih kategori produk terlebih dahulu", Toast.LENGTH_SHORT).show();
            focusView = spKategori;
            cancel = true;
        } else if (RbHelper.isEmpty(txtKeyword)) {
            txtKeyword.setError("Harus di isi !!");
            focusView = txtKeyword;
            cancel = true;
        }
        if (cancel) {
            focusView.requestFocus();
        } else {

            KEYWORD = txtKeyword.getText().toString();

            String url = "http://ibacor.com/api/produk-halal-mui?menu=" + KATEGORI + "&query=" + KEYWORD +
                    "&page=" + PAGE;
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            RbHelper.pre("url " + url);
            showLoading();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, final IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            RbHelper.pesan(c, e.getMessage());
                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    if (!response.isSuccessful()) {
                        hideLoading();
                        throw new IOException("unexpected code" + response);
                    }
                    final String responData = response.body().string();
                    RbHelper.pre("respone " + responData);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                hideLoading();
                                JSONObject json = new JSONObject(responData);
                                //chek result
                                String status = json.getString("status");
                                //String msg = json.getString("msg");
                                if (status.equalsIgnoreCase("success")) {
                                    JSONArray jsonArray = json.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jobj = jsonArray.getJSONObject(i);
                                        ProdukModel x = new ProdukModel();
                                        x.setNama_produk(jobj.getString("nama_produk"));
                                        x.setNama_produsen(jobj.getString("nama_produsen"));
                                        x.setNomor_sertifikat(jobj.getString("nomor_sertifikat"));
                                        x.setBerlaku_hingga(jobj.getString("berlaku_hingga"));

                                        RbHelper.pre("Contoh : " + x.getNama_produk());

                                        ProdukModel.add(x);
                                    }
                                    Toast.makeText(c, "Sukses, gulir ke bawah untuk melihat daftar produk", Toast.LENGTH_SHORT).show();

                                } else if (status.equalsIgnoreCase("error")) {
                                    Toast.makeText(c, "Tidak ditemukan produk dengan kata kunci " + KEYWORD, Toast.LENGTH_LONG).show();
                                }

                                RbHelper.pre("list model : " + ProdukModel);

                                adapter = new ProdukAdapter(c, ProdukModel);
                                rvProduk.setAdapter(adapter);

                            } catch (JSONException e) {
                                e.printStackTrace();
                                RbHelper.pesan(c, "Maaf server sedang gangguan, silahkan coba beberapa saat lagi");
                            } catch (Exception e) {
                                e.printStackTrace();
                                RbHelper.pesan(c, "Error mengambil data");
                            }
                        }
                    });
                }
            });
        }
    }

    @Override
    public void onPause() {
        if (adBawah != null) {
            adBawah.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adBawah != null) {
            adBawah.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (adBawah != null) {
            adBawah.destroy();
        }
        super.onDestroy();
    }
}
