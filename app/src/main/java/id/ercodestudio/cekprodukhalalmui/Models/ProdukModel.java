package id.ercodestudio.cekprodukhalalmui.Models;

/**
 * Created by Ariev27 on 14/03/2017.
 */

public class ProdukModel {
    private String nama_produk, nomor_sertifikat, nama_produsen, berlaku_hingga;


    public String getNama_produk() {
        return nama_produk;
    }

    public void setNama_produk(String nama_produk) {
        this.nama_produk = nama_produk;
    }

    public String getNomor_sertifikat() {
        return nomor_sertifikat;
    }

    public void setNomor_sertifikat(String nomor_sertifikat) {
        this.nomor_sertifikat = nomor_sertifikat;
    }

    public String getNama_produsen() {
        return nama_produsen;
    }

    public void setNama_produsen(String nama_produsen) {
        this.nama_produsen = nama_produsen;
    }

    public String getBerlaku_hingga() {
        return berlaku_hingga;
    }

    public void setBerlaku_hingga(String berlaku_hingga) {
        this.berlaku_hingga = berlaku_hingga;
    }
}
