package id.ercodestudio.cekprodukhalalmui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Created by Ariev27 on 12/20/16
 * Ariev27@live.com
 */

public class BaseActivity extends AppCompatActivity {
    protected Context c;
    protected AlphaAnimation btnAnimasi = new AlphaAnimation(1F, 0.7F);

    protected TextView tvTitle;
    protected OkHttpClient client;
    protected ProgressDialog dialog;


    //protected DrawerBuilder result = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        c = this;
        dialog = new ProgressDialog(c);

        client = new OkHttpClient.Builder()
                .connectTimeout(90, TimeUnit.SECONDS)
                .writeTimeout(90, TimeUnit.SECONDS)
                .readTimeout(90, TimeUnit.SECONDS)
                .build();

    }

    public void showLoading() {

        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setMessage("Loading...");

        dialog.show();








    }

    public void hideLoading() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    protected void showHeader() {
        ActionBar actionBar = getSupportActionBar();
        //actionBar.setLogo(R.drawable.logo_login);
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false); //optional
        actionBar.setDisplayShowTitleEnabled(false); //optional
    }

    /*
    public void dialogKeluar() {
        AlertDialog.Builder dialogz = new AlertDialog.Builder(
                c);
        dialogz.setMessage("Anda yakin mau keluar ?");
        dialogz.setTitle("Konfirmasi");
        dialogz.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //removeGCM();
                sesi.logoutUser2();
                LoginManager.getInstance().logOut();


                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                // Closing all the Activities
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                // Add new Flag to start new Activity
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();


            }
        });
        dialogz.setNegativeButton("Tidak",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogx, int id) {
                        dialogx.cancel();
                    }
                });
        dialogz.show();
    }
    */

/*
    protected void drawerMenu(int posisi) {

        Toolbar toolbar = (Toolbar) this.findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);

//        getSupportActionBar().setHomeButtonEnabled(true);
//        getSupportActionBar().setTitle(Html.fromHtml("<font color='#FFFFFF'>Catatan Ibadah Harian </font>"));
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        tvTitle = (TextView) findViewById(R.id.tvJudul);


        SecondaryDrawerItem home = new SecondaryDrawerItem().withName("Home")
                .withIcon(GoogleMaterial.Icon.gmd_home).withTag("home");

        SecondaryDrawerItem laporan = new SecondaryDrawerItem().withName("Laporan \'Amaliyah")
                .withIcon(GoogleMaterial.Icon.gmd_insert_chart).withTag("laporan");

        SecondaryDrawerItem edit = new SecondaryDrawerItem().withName("Data \'Amaliah")
                .withIcon(GoogleMaterial.Icon.gmd_event).withTag("edit");

        SecondaryDrawerItem pengaturan = new SecondaryDrawerItem().withName("Pengaturan")
                .withIcon(GoogleMaterial.Icon.gmd_settings).withTag("pengaturan");

        SecondaryDrawerItem about = new SecondaryDrawerItem().withName("About")
                .withIcon(GoogleMaterial.Icon.gmd_info).withTag("about");


        //byte[] decodedString = Base64.decode(sesi.getAvatar(), Base64.DEFAULT);
        //Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.drawer_header)
                .build();

        //create the drawer and remember the `Drawer` result object
        result = new DrawerBuilder();

        result.withAccountHeader(headerResult)
                .withActivity(this)
                .withToolbar(toolbar);

        if (true) {
            result.addDrawerItems(
                    home,
                    edit,
                    laporan,
                    new SectionDrawerItem().withName("Account"),
                    pengaturan,
                    about
            ).withSelectedItemByPosition(posisi).build();

        }


        result.withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
            @Override
            public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                Intent i = null;

                String judul = (String) drawerItem.getTag();

                switch (judul) {
                    case "home":
                        i = new Intent(c, MainActivity.class);
                        startActivity(i);
                        finish();
                        break;

                    case "laporan":
                        i = new Intent(c, LaporanActivity.class);
                        startActivity(i);
                        finish();
                        break;

                    case "edit":
                        i = new Intent(c, DataAktifitasActivity.class);
                        startActivity(i);
                        finish();
                        break;

                    case "pengaturan":
                        i = new Intent(c, PengaturanActivity.class);
                        startActivity(i);
                        finish();
                        break;

                    case "about":
                        //i = new Intent(c, LokasiSayaActivity.class);
                        startActivity(i);
                        finish();
                        break;

                }

                return true;

            }
        });

        result.withSelectedItem(0);

    }
    */

}