package id.ercodestudio.cekprodukhalalmui.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import id.ercodestudio.cekprodukhalalmui.Models.ProdukModel;
import id.ercodestudio.cekprodukhalalmui.R;

/**
 * Created by Ariev27 on 17/3/17
 * Ariev27@live.com
 */

public class ProdukAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<ProdukModel> data;
    private Context c;

    public ProdukAdapter(Context c, ArrayList<ProdukModel> data) {
        this.data = data;
        this.c = c;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(c).inflate(R.layout.list_data, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ProdukModel x = data.get(position);
        MyViewHolder mholder = (MyViewHolder) holder;
        mholder.tvNamaProduk.setText(x.getNama_produk());
        mholder.tvNamaProdusen.setText(x.getNama_produsen());
        mholder.tvNomorSertifikat.setText(x.getNomor_sertifikat());
        mholder.tvBerlakuHingga.setText(x.getBerlaku_hingga());

    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    //sisipkan view holder
    private class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvNamaProduk, tvNamaProdusen, tvNomorSertifikat, tvBerlakuHingga;
        LinearLayout namaProdukContainer;

        MyViewHolder(View v) {
            super(v);
            tvNamaProduk = (TextView) v.findViewById(R.id.tvNamaProduk);
            tvNamaProdusen = (TextView) v.findViewById(R.id.tvNamaProdusen);
            tvNomorSertifikat = (TextView) v.findViewById(R.id.tvNomorSertifikat);
            tvBerlakuHingga = (TextView) v.findViewById(R.id.tvBerlakuHingga);
            namaProdukContainer = (LinearLayout)v.findViewById(R.id.containerNamaProduk);
        }
    }
}